# 2022_formation_Scout_QField
# Explications
Formation d'initiation aux outils de collecte Scout et Qfield sur tablette et ordiphone.

## Racine 
- Licence 
- Diaporama


## 01_scout
### Tutoriels
- 1_Scout_livret_tuto_preparation_terrain.pdf
- 2_Scout_livret_tuto_collecte_terrain.pdf
- 3_Scout_livret_tuto_retour_terrain.pdf
### Divers repertoires de ressources
- Liste_metier
- Rapport_personnalise
- Visite_scout_avec_liste_metier


## 02_qfield
- Livret_Qfield_sortie_1.pdf
- Memo_terrain_QField_objets_points.pdf
- Livret_Qfield_sortie_2.pdf
- Memo_terrain_QField_objets_polygones_lignes.pdf
  
### Projets QField corrigés ( pour QGIS v3.10)
- collecte_1 (Autun / Bron / Clermont / Isle d'Abeau)
- collecte_2 (Autun / Bron / Clermont / Isle d'Abeau)


### 99_autres_ressources
- antiseche_QGIS
- liens vers documentation officielle

le 24/10/2022