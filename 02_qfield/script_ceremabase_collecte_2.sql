--CREATE SCHEMA w_formation;

DROP TABLE IF EXISTS w_formation.inventaire_parking;

CREATE TABLE w_formation.inventaire_parking 
( id serial,
nom varchar(254),
surface_calculee integer,
--revetement varchar(10),
collecteur varchar(20),
geom geometry('POLYGON',2154)
);

GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire;
GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire1;
GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire2;
GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire3;
GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire4;
GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire5;
GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire6;
GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire7;
GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire8;
GRANT ALL ON TABLE w_formation.inventaire_parking TO stagiaire9;

ALTER TABLE IF EXISTS w_formation.inventaire_parking
    ADD CONSTRAINT inventaire_parking_id_pk PRIMARY KEY (id);